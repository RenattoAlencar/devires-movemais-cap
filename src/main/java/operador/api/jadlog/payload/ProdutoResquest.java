package operador.api.jadlog.payload;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class ProdutoResquest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String conteudo;
	private List<String> pedido;
	private Double totPeso;
	private Double totValor;
	private String obs;
	private Integer modalidade;
	private String contaCorrente;
	private String tpColeta;
	private Integer tipoFrete;
	private String cdUnidadeOri;
	private String cdUnidadeDes;
	private String cdPickupOri;
	private String cdPickupDes;
	private Integer nrContrato;
	private Integer servico;
	private String shipmentId;
	private Double vlColeta;
	private String nome;
	private String cnpjCpf;
	private String ie;
	private String endereco;
	private String numero;
	private String compl;
	private String bairro;
	private String cidade;
	private String uf;
	private String cep;
	private String fone;
	private String cel;
	private String email;
	private String contato;
	private String cfop;
	private String danfeCte;
	private String nrDoc;
	private String serie;
	private Integer tpDocumento;
	private Double valor;
	private Integer altura;
	private Integer comprimento;
	private String identificador;
	private Integer largura;
	private Double peso;


}
