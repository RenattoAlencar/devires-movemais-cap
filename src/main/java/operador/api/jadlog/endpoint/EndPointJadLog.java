package operador.api.jadlog.endpoint;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.Api;
import operador.api.jadlog.payload.ConsultaRequest;
import operador.api.jadlog.payload.ConsultaResponse;
import operador.api.jadlog.payload.ProdutoResponse;
import operador.api.jadlog.payload.ProdutoResquest;
import operador.api.jadlog.service.ConsultarService;
import operador.api.jadlog.service.ProdutoService;

@Api("JadLog")
@RestController
@RequestMapping()
public class EndPointJadLog {

	@Autowired
	private ProdutoService produtoservice;
	
	@Autowired
	private ConsultarService consultaservice;
		
	@PostMapping("/pedidos")
	public ResponseEntity<ProdutoResponse> post(@RequestBody @Valid ProdutoResquest input,
			UriComponentsBuilder uriBuilder) {
		ProdutoResponse output = produtoservice.jadlog(input);
		URI uri = uriBuilder.path("/pedidos").buildAndExpand(output).toUri();
		return ResponseEntity.created(uri).body(output);
	}

	
	@PostMapping("/consultar")
	public ResponseEntity<ConsultaResponse> post(@RequestBody @Valid ConsultaRequest input,
			UriComponentsBuilder uriBuilder){
		ConsultaResponse output = consultaservice.jadlog(input);
		URI uri = uriBuilder.path("/consultar").buildAndExpand(output).toUri();
		return ResponseEntity.created(uri).body(output);
	}
	
}
