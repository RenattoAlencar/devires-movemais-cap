package operador.api.jadlog.integracao.payload;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogDfeRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String danfeCte;
	private String nrDoc;
	private String serie;
	private Double valor;
	private String cfop;
	private Integer tpDocumento;
	
}
