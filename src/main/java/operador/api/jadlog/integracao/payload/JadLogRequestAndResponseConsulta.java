package operador.api.jadlog.integracao.payload;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogRequestAndResponseConsulta {

	private String codigo;
	private JadLogResponseStatusTracking tracking;
	private List<JadLogResponseStatusVolume> volume;


}
