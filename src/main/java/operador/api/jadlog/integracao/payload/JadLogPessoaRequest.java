package operador.api.jadlog.integracao.payload;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogPessoaRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nome;
	private String cnpjCpf;
	private String ie;
	private String endereco;
	private String numero;
	private String compl;
	private String bairro;
	private String cidade;
	private String uf;
	private String cep;
	private String fone;
	private String email;
	private String contato;
	
	

}
