package operador.api.jadlog.integracao.payload;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogVolumeRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer altura;
	private Integer comprimento;
	private Integer largura;
	private Double peso;
	private String identificador;
	
}
