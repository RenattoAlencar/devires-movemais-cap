package operador.api.jadlog.integracao.payload;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogResponseStatusEvento {

		private LocalDateTime data;
		private String status;
		private String unidade;
	
}
