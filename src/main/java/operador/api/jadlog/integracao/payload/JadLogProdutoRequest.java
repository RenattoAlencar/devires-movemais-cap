package operador.api.jadlog.integracao.payload;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogProdutoRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String conteudo; 
	private List<String> pedido;
	private Double totPeso;
	private Double totValor;
	private String obs;
	private Integer modalidade;
	private String contaCorrente;
	private String tpColeta;
	private Integer tipoFrete;
	private String cdUnidadeOri;
	private String cdUnidadeDes;
	private String cdPickupOri;
	private String cdPickupDes;
	private Integer nrContrato;
	private Integer servico;
	private String shipmentId;
	private Double vlColeta;
	
	@JsonAlias("dfe")
	@JsonProperty("dfe")
	private List<JadLogDfeRequest> dfe;
	
	private List<JadLogVolumeRequest> volume;
	
	@JsonAlias("rem")
	@JsonProperty("rem")
	private JadLogPessoaRequest remetente;
	
	@JsonAlias("des")
	@JsonProperty("des")
	private JadLogPessoaRequest destinatario;

}
