package operador.api.jadlog.integracao.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogResponseStatusVolume {

	private Integer altura;
	private Integer comprimento;
	private Integer largura;
	private Double peso;

}
