package operador.api.jadlog.integracao;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import operador.api.jadlog.integracao.payload.JadLogProdutoRequest;
import operador.api.jadlog.integracao.payload.JadLogProdutoResponse;
import operador.api.jadlog.integracao.payload.JadLogRequestStatus;
import operador.api.jadlog.integracao.payload.JadLogResponseStatus;

@Slf4j
@Component
public class IntegracaoCliente {

	@Value("${operador.api.jadlog.integracao.token}")
	private String token;

	@Value("${operador.api.jadlog.integracao.endpoint}")
	private String ENDPOINT;

	@Value("${operador.api.jadlog.integracao.endpointtracking}")
	private String ENDPOINT_TRACKING;

	private RestTemplate restTemplate;

	@PostConstruct
	public void init() {
		restTemplate = new RestTemplateBuilder().additionalInterceptors(HeaderToken.builder().token(token).build())
				.build();
	}

	public JadLogProdutoResponse run(JadLogProdutoRequest input) {
		try {
			String path = String.format("%s/incluir", ENDPOINT);
			HttpEntity<JadLogProdutoRequest> request = new HttpEntity<JadLogProdutoRequest>(input);
			ResponseEntity<JadLogProdutoResponse> response = restTemplate.postForEntity(path, request, JadLogProdutoResponse.class);
			log.info("Response: {}", response);
			return response.getBody();

		} catch (HttpClientErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		} catch (HttpServerErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		}
	}


	public JadLogResponseStatus run(JadLogRequestStatus input) {
		try {
			String path = String.format("%s/consultar", ENDPOINT_TRACKING);
			HttpEntity<JadLogRequestStatus> request = new HttpEntity<JadLogRequestStatus>(input);
			ResponseEntity<JadLogResponseStatus> response = restTemplate.postForEntity(path, request,
					JadLogResponseStatus.class);
			log.info("Response: {}", response);
			return response.getBody();

		} catch (HttpClientErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		} catch (HttpServerErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;

		}

	}

}
