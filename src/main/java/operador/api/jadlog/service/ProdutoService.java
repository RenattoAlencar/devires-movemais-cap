package operador.api.jadlog.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import operador.api.jadlog.integracao.IntegracaoCliente;
import operador.api.jadlog.integracao.payload.JadLogDfeRequest;
import operador.api.jadlog.integracao.payload.JadLogPessoaRequest;
import operador.api.jadlog.integracao.payload.JadLogProdutoRequest;
import operador.api.jadlog.integracao.payload.JadLogProdutoResponse;
import operador.api.jadlog.integracao.payload.JadLogVolumeRequest;
import operador.api.jadlog.payload.ProdutoResponse;
import operador.api.jadlog.payload.ProdutoResquest;

@Service
public class ProdutoService {

	@Value("${operador.jadlog.nome}")
	private String nome;

	@Value("${operador.jadlog.cnpjCpf}")
	private String cnpjCpf;

	@Value("${operador.jadlog.ie}")
	private String ie;

	@Value("${operador.jadlog.endereco}")
	private String endereco;

	@Value("${operador.jadlog.numero}")
	private String numero;

	@Value("${operador.jadlog.compl}")
	private String compl;

	@Value("${operador.jadlog.bairro}")
	private String bairro;

	@Value("${operador.jadlog.cidade}")
	private String cidade;

	@Value("${operador.jadlog.uf}")
	private String uf;

	@Value("${operador.jadlog.cep}")
	private String cep;

	@Value("${operador.jadlog.fone}")
	private String fone;

	@Value("${operador.jadlog.email}")
	private String email;

	@Value("${operador.jadlog.contato}")
	private String contato;

	@Autowired
	private IntegracaoCliente integracaoCliente;

	public ProdutoResponse jadlog(ProdutoResquest input) {
		JadLogProdutoRequest request = transform(input);
		JadLogProdutoResponse response = integracaoCliente.run(request);
		ProdutoResponse output = transform(response);
		return output;

	}

	private JadLogProdutoRequest transform(ProdutoResquest input) {
		return JadLogProdutoRequest.builder().conteudo(input.getConteudo()).pedido(input.getPedido())
				.totPeso(input.getTotPeso()).totValor(input.getTotValor()).obs(input.getObs())
				.modalidade(input.getModalidade()).contaCorrente(input.getContaCorrente()).tpColeta(input.getTpColeta())
				.tipoFrete(input.getTipoFrete()).cdUnidadeOri(input.getCdUnidadeOri())
				.cdUnidadeDes(input.getCdUnidadeDes()).cdPickupOri(input.getCdPickupOri())
				.cdPickupDes(input.getCdPickupDes()).nrContrato(input.getNrContrato()).servico(input.getServico())
				.shipmentId(input.getShipmentId()).vlColeta(input.getVlColeta())
				.destinatario(JadLogPessoaRequest.builder().nome(input.getNome()).cnpjCpf(input.getCnpjCpf())
						.ie(input.getIe()).endereco(input.getEndereco()).numero(input.getNumero())
						.compl(input.getCompl()).bairro(input.getBairro()).cidade(input.getCidade()).uf(input.getUf())
						.cep(input.getCep()).fone(input.getFone()).email(input.getEmail()).contato(input.getContato())
						.build())
				.remetente(JadLogPessoaRequest.builder().nome(nome).cnpjCpf(cnpjCpf).ie(ie).endereco(endereco)
						.numero(numero).compl(compl).bairro(bairro).cidade(cidade).uf(uf).cep(cep).fone(fone)
						.email(email).contato(contato).build())
				.dfe(Arrays.asList(JadLogDfeRequest.builder().danfeCte(input.getDanfeCte()).nrDoc(input.getNrDoc())
						.serie(input.getSerie()).valor(input.getValor()).cfop(input.getCfop())
						.tpDocumento(input.getTpDocumento()).build()))
				.volume(Arrays.asList(JadLogVolumeRequest.builder().altura(input.getAltura())
						.comprimento(input.getComprimento()).largura(input.getLargura()).peso(input.getPeso())
						.identificador(input.getIdentificador()).build()))

				.build();

	}

	private ProdutoResponse transform(JadLogProdutoResponse response) {
		return ProdutoResponse.builder().codigo(response.getCodigo()).shipmentId(response.getShipmentId())
				.status(response.getStatus()).build();
	}
}
