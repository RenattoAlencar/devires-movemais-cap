package operador.api.jadlog.service;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import operador.api.jadlog.integracao.IntegracaoCliente;
import operador.api.jadlog.integracao.payload.JadLogRequestAndResponseConsulta;
import operador.api.jadlog.integracao.payload.JadLogRequestStatus;
import operador.api.jadlog.integracao.payload.JadLogResponseStatus;
import operador.api.jadlog.payload.ConsultaRequest;
import operador.api.jadlog.payload.ConsultaResponse;

@Service
public class ConsultarService {

	@Autowired
	private IntegracaoCliente integracaoCliente;

	public ConsultaResponse jadlog(ConsultaRequest input) {
		JadLogRequestStatus request = transform(input);
		JadLogResponseStatus response = integracaoCliente.run(request);
		ConsultaResponse output = transform(response);
		return output;
	}

	private JadLogRequestStatus transform(ConsultaRequest input) {
		return JadLogRequestStatus.builder()
				.consulta(Arrays.asList(JadLogRequestAndResponseConsulta.builder().codigo(input.getCodigo()).build()))
				.build();

	}
	

	private ConsultaResponse transform(JadLogResponseStatus input) {
		if (input != null && !CollectionUtils.isEmpty(input.getConsulta())) {
			Optional<JadLogRequestAndResponseConsulta> consulta = input.getConsulta().stream().findFirst();
			if (consulta.isPresent()) {
				return ConsultaResponse.builder().codigo(consulta.get().getCodigo()).build();
			}

		}

		throw new RuntimeException("Codigo não encontrado");

	}
}