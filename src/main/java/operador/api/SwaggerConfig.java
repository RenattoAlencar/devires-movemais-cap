package operador.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	private final static String BASE_PACKAGE = "operador.api.correios.endpoint";
	private final static String PATH_SELECTORS = "/.*";

	@Bean
	public Docket configDocket() {

		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
				.paths(PathSelectors.regex(PATH_SELECTORS))
				.build()
				.apiInfo(new ApiInfoBuilder()
						.title("Operadores Logisticos")
						.description("Buscar operadores Logisticos e Validar os servicos").version("1.0.0")
						.contact(new Contact("Architectural team", "GIT_URL", "")).build())
				.useDefaultResponseMessages(false);
	}

}
