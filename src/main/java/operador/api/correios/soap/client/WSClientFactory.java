package operador.api.correios.soap.client;

import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.handler.Handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class WSClientFactory {
	
	private static final String BINDING_PROPERTY_CONNECT_TIMEOUT = "com.sun.xml.ws.connect.timeout";
	private static final String BINDING_PROPERTY_REQUEST_TIMEOUT = "com.sun.xml.ws.request.timeout";
	private static final int CONNECT_TIMEOUT_DEFAULT = 5 * 1000;  // 05 segs
	private static final int REQUEST_TIMEOUT_DEFAULT = 30 * 1000; // 30 segs

	@Autowired
	private Environment env;
	
	@SuppressWarnings("rawtypes")
	public <SVC extends Service, PT> PT create(Class<SVC> clientClazz, Class<PT> portClazz) {
		
		WebServiceClient clientAnnotation = clientClazz.getAnnotation(WebServiceClient.class);
		QName serviceNamespace = new QName(clientAnnotation.targetNamespace(), clientAnnotation.name());
		URL wsdlLocation = clientClazz.getClassLoader().getResource(clientAnnotation.wsdlLocation());
		String serviceClazzName = clientClazz.getSimpleName();
		
		String username = env.getProperty(serviceClazzName + ".username");
		String password = env.getProperty(serviceClazzName + ".password");
		String endpoint = env.getProperty(serviceClazzName + ".endpoint");
		int requestTimeout = env.getProperty(serviceClazzName + ".requestTimeout") == null ? REQUEST_TIMEOUT_DEFAULT : Integer.parseInt(env.getProperty(serviceClazzName + ".requestTimeout"));
		int connectTimeout = env.getProperty(serviceClazzName + ".connectTimeout") == null ? CONNECT_TIMEOUT_DEFAULT : Integer.parseInt(env.getProperty(serviceClazzName + ".connectTimeout"));
		
		SVC client;
		try {
			Constructor<SVC> constructor = clientClazz.getConstructor(URL.class, QName.class);
			client = constructor.newInstance(wsdlLocation, serviceNamespace);
		} catch (Exception e) {
			throw new RuntimeException("Error creating WS Client", e);
		}

		PT port = client.getPort(portClazz);
		BindingProvider binding = (BindingProvider) port;
		ArrayList<Handler> chain = new ArrayList<>();

		// WS-Security Handler
		/*if (username != null && password != null) {
			 chain.add(new WSSOAPHandler(username, password));
		}*/

		/*
		 * // Log Envelope e Fault Handlers chain.add(new LogEnvelopeSOAPHandler());
		 * chain.add(new LogFaultSOAPHandler());
		 */
		binding.getBinding().setHandlerChain(chain);

		binding.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
		binding.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
		binding.getRequestContext().put(BINDING_PROPERTY_CONNECT_TIMEOUT, connectTimeout);
		binding.getRequestContext().put(BINDING_PROPERTY_REQUEST_TIMEOUT, requestTimeout);

		return port;
	}

}
