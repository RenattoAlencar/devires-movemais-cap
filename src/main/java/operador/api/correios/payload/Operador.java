package operador.api.correios.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)
public class Operador {

	@ApiModelProperty(example="1")
	private Long idOperador;
	
	@ApiModelProperty(example="Correios")
	private String nome;
	

}