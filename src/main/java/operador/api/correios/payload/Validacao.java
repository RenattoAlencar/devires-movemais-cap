package operador.api.correios.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Validacao {
	
	@ApiModelProperty(example="10")
	private Long idServico;
	
	@ApiModelProperty(example="2")
	private Long idOperador;
	
	@ApiModelProperty(example="05311900")
	private String cepDestino;
	
	@ApiModelProperty(example="05311900")
	private String cepOrigem;

}
