package operador.api.correios.integracao.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class BuscaClienteRequest {

	@ApiModelProperty(example = "5")
	private String idContrato;

	@ApiModelProperty(example = "3")
	private String idCartaoPostagem;

}
