package operador.api.correios.integracao.payload;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class BuscaClienteResponse {
	
	@ApiModelProperty(example="91449522000120")
	private String cnpj;
	
	@ApiModelProperty(example="17000190")
	private String codAdministrativo;
	
	private List<CorreioServicos> servicosCorreio;
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
