package operador.api.correios.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import operador.api.correios.integracao.payload.BuscaClienteResponse;
import operador.api.correios.integracao.payload.VerificaServicoRequest;
import operador.api.correios.payload.Operador;
import operador.api.correios.service.CorreiosService;
import operador.api.correios.service.ListaOperadores;
import operador.api.correios.service.VerificaService;
import operador.api.correios.soap.wsdl.AutenticacaoException;
import operador.api.correios.soap.wsdl.SigepClienteException;

@Api(tags = "operadores")
@RestController
@RequestMapping("operadores")
public class OperadoresEndpoint {
	
	@Autowired
	private CorreiosService correiosService;

	@Autowired
	private VerificaService verificaService;
	
	@Autowired
	private ListaOperadores listaOperadores;
	
	@GetMapping("{id}/servicos")
	@ApiOperation(value = "Servicos do correios", notes = "Lista todos os servicos do correios")
	@ApiResponses({ 
		@ApiResponse(code = 200, message = ""),
		@ApiResponse(code = 412, message = "")
	})
	public ResponseEntity<BuscaClienteResponse> verificarServico(@PathVariable("id") Long id) throws AutenticacaoException, SigepClienteException {
		BuscaClienteResponse resp = correiosService.buscar(id); 
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	@PostMapping("disponibilidade")
	@ApiOperation(value = "Disponibilidade", notes = "Verifica a Disponibiliadade do Servico do Correios para os parametros CEP Origem e Destino")
	public ResponseEntity<String> disponibilidade(@RequestBody VerificaServicoRequest input){
		String resposta = verificaService.validar(input);
		return ResponseEntity.status(HttpStatus.OK).body(resposta);
	}
	
	@GetMapping()
	@ApiOperation(value = "Listar operadores", notes = "Lista os operadores logisticos")
	public ResponseEntity<List<Operador>> buscarOperadores() throws JsonProcessingException  {
		return ResponseEntity.ok( listaOperadores.listar());
	}

}
