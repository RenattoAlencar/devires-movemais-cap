package operador.api.correios.json;

import java.time.LocalTime;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class LocalTimeModule extends SimpleModule {
	private static final long serialVersionUID = 1L;

	public LocalTimeModule() {
		super();
		this.addSerializer(LocalTime.class, new LocalTimeSerializer());
		this.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
	}
	
}

