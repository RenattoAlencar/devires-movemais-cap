package operador.api.correios.json;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtil {
	private static final ObjectMapper OBJECT_MAPPER;
	
	static {
		OBJECT_MAPPER = new ObjectMapper();
		OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
		OBJECT_MAPPER.setSerializationInclusion(Include.NON_EMPTY);
		OBJECT_MAPPER.registerModule(new LocalTimeModule());
	}
	
	public static JsonNode fromJSON(String json) throws JsonProcessingException {
		if (StringUtils.isBlank(json)) {
			return null;
		}
		try {
			return OBJECT_MAPPER.readTree(json);
		} catch (IOException e) {
			// origem string nao deve causar erro de IO
			throw new RuntimeException(e);
		}
	}
	
	public static <T> T fromJSON(String json, Class<T> clazz) throws JsonProcessingException {
		if (StringUtils.isBlank(json)) {
			return null;
		}
		try {
			return OBJECT_MAPPER.readValue(json, clazz);
		} catch (IOException e) {
			// origem string nao deve causar erro de IO
			throw new RuntimeException(e);
		}
	}
	
	public static <T> T fromJSON(InputStream json, Class<T> clazz) throws JsonProcessingException {
		try {
			return OBJECT_MAPPER.readValue(json, clazz);
		} catch (IOException e) {
			// origem string nao deve causar erro de IO
			throw new RuntimeException(e);
		}
	}
	
	public static <T> T fromJSONQuietly(String json, Class<T> clazz)  {
		try {
			return JSONUtil.fromJSON(json, clazz);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static JsonNode fromJSONQuietly(String json)  {
		try {
			return JSONUtil.fromJSON(json);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String toJSON(Object obj) throws JsonProcessingException {
		if (obj == null) {
			return null;
		}
		return OBJECT_MAPPER.writeValueAsString(obj);
	}
	
	public static String toJSONQuietly(Object obj) {
		if (obj == null) {
			return null;
		}
		try {
			return OBJECT_MAPPER.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
