package operador.api.correios.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import operador.api.correios.integracao.payload.VerificaServicoRequest;
import operador.api.correios.soap.client.WSClientFactory;
import operador.api.correios.soap.wsdl.AtendeCliente;
import operador.api.correios.soap.wsdl.AtendeClienteService;
import operador.api.correios.soap.wsdl.AutenticacaoException;
import operador.api.correios.soap.wsdl.SigepClienteException;

@Service
public class VerificaService {
	
	@Value("${AtendeClienteService.username}")
	private String usuario;
	
	@Value("${AtendeClienteService.password}")
	private String senha;
	
	@Autowired
	private WSClientFactory wsClientFactory;
	
	public String validar(VerificaServicoRequest input) {
		String verificaDisponibilidadeServico = null;
		try {
			AtendeCliente atendeCliente = wsClientFactory.create(AtendeClienteService.class, AtendeCliente.class);
			verificaDisponibilidadeServico = atendeCliente.verificaDisponibilidadeServico(Integer.parseInt(input.getCodAdministrativo()), input.getNumeroServico(), input.getCepOrigem(), input.getCepDestino(), usuario, senha);
		}  catch (AutenticacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SigepClienteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return verificaDisponibilidadeServico;
	}
	
}
