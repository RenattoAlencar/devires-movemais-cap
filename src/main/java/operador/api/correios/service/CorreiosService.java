package operador.api.correios.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.val;
import lombok.extern.slf4j.Slf4j;
import operador.api.correios.integracao.payload.BuscaClienteResponse;
import operador.api.correios.integracao.payload.CorreioServicos;
import operador.api.correios.soap.client.WSClientFactory;
import operador.api.correios.soap.wsdl.AtendeCliente;
import operador.api.correios.soap.wsdl.AtendeClienteService;
import operador.api.correios.soap.wsdl.AutenticacaoException;
import operador.api.correios.soap.wsdl.ClienteERP;
import operador.api.correios.soap.wsdl.ContratoERP;
import operador.api.correios.soap.wsdl.SigepClienteException;

@Slf4j
@Service
public class CorreiosService {

	@Value("${AtendeClienteService.username}")
	private String usuario;

	@Value("${AtendeClienteService.password}")
	private String senha;

	@Value("${operador.correios.data.contrato}")
	private String contrato;

	@Value("${operador.correios.data.cartaoPostagem}")
	private String cartaoPostagem;

	@Value("${operador.correios.data.id}")
	private Long idCorreios;

	@Autowired
	private WSClientFactory wsClientFactory;

	public BuscaClienteResponse buscar(Long id) throws AutenticacaoException, SigepClienteException {

		if (idCorreios.equals(id)) {
			return buscaServicosCorreios();
		}

		return BuscaClienteResponse.builder().servicosCorreio(new ArrayList<CorreioServicos>()).build();
	}

	private BuscaClienteResponse buscaServicosCorreios() throws AutenticacaoException, SigepClienteException {
		try {

			AtendeCliente atendeCliente = wsClientFactory.create(AtendeClienteService.class, AtendeCliente.class);
			ClienteERP buscaCliente = atendeCliente.buscaCliente(contrato, cartaoPostagem, usuario, senha);

			val buscaClienteResp = BuscaClienteResponse.builder();
			buscaClienteResp.cnpj(buscaCliente.getCnpj().trim());

			Optional<ContratoERP> contratoERPOp = buscaCliente.getContratos().stream().findFirst();
			if (contratoERPOp.isPresent()) {
				ContratoERP contratoERP = contratoERPOp.get();
				contratoERP.getCartoesPostagem().stream().findFirst().ifPresent(cartaoPostagem -> {

					List<CorreioServicos> correiosServicosList = cartaoPostagem.getServicos().stream().map(servico -> {
						val correioServicos = CorreioServicos.builder().id(servico.getId())
								.descricao(servico.getDescricao().trim()).build();
						return correioServicos;
					}).collect(Collectors.toList());

					buscaClienteResp.codAdministrativo(cartaoPostagem.getCodigoAdministrativo().trim());
					buscaClienteResp.servicosCorreio(correiosServicosList);
				});
			}

			return buscaClienteResp.build();

		} catch (AutenticacaoException e) {
			log.debug("${ERRO:}", e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (SigepClienteException e) {
			log.debug("${ERRO:}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}

}
