package operador.api.correios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import operador.api.correios.json.JSONUtil;
import operador.api.correios.payload.Operador;
import operador.api.correios.payload.Operadores;


@Service
public class ListaOperadores {

	@Value("${operadores.parceiros.data}")
	private String data;

	public List<Operador> listar() throws JsonProcessingException {
		Operadores operadores = JSONUtil.fromJSON(data, Operadores.class);
		return operadores.getData();

	}

}
