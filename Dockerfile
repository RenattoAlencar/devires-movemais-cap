### BUILD image
FROM maven:3.6.1-jdk-8 as maven
ARG MAVEN_OPTS

COPY . /usr/src
RUN mvn -B -Dmaven.artifact.threads=10 clean package spring-boot:repackage -f /usr/src/pom.xml

FROM openjdk:8-jdk-slim
COPY --from=maven /usr/src/target/operadorlogisticos.api-*.jar /app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]